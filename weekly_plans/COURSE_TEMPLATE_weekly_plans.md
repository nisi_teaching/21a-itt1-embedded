---
title: 'COURSE TEMPLATE'
subtitle: 'Weekly plans'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: 'COURSE TEMPLATE, weekly plans'
---


Introduction
====================

This document is a collection of weekly plans. It is based on the wekly plans in the administrative repository, and is updated automatically on change.

The sections describe the goals and programm for each week of the second semester project.



# Week XX

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* ..
* ..

### Learning goals
* ..
* ..

## Deliverables
* ..
* ..


## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Tuesday

| Time | Activity |
| :---: | :--- |
| 8:15 | ... |
| 9:00 |  You work |

### Wednesday

| Time | Activity |
| :---: | :--- |
| 8:15  | ... |
| 12:30 | Presentations and discussions |



## Hands-on time

### Exercise 0

![this is an example image](ucl_logo_raw.png)

...

### Exercise 1

...

### Exercise 2

...

## Comments
* ..
* ..


# Additional resources
None at this time
