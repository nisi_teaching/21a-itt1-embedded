---
Week: 43
Content: RPi setup
Material: See links in weekly plan
Initials: NISI
---

# Week 43

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* CLI cheatsheet created and stored at gitlab
* SSH access to RPi established
* SSH user agent setup

### Learning goals
* SSH and user agent
    * Level 1: The student knows the purpose of user agent forwarding
    * Level 2: The student can use the CLI to copy from local to remote machine
    * Level 3: The student can use SSH agent forwarding to access remote machines 
* Linux shell commands
    * Level 1: The student knows basic shell commands to navigate folders on Linux
    * Level 2: The student can use basic shell commands on Linux
    * Level 3: The student can individually aquire new knowledge about shell commands

## Schedule

Online at zoom: [https://ucldk.zoom.us/j/62627436603](https://ucldk.zoom.us/j/62627436603) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 8:15 Introduction (K1)
* 9:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises with lecturer (K4)
* 13:00 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Exercises

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww43](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww43)

## Comments
* Book - Conquer the Command Line [https://magpi.raspberrypi.com/books/command-line-second-edition](https://magpi.raspberrypi.com/books/command-line-second-edition)
* Book - The Official Raspberry Pi Handbook 2022  [https://magpi.raspberrypi.com/books/handbook-2022](https://magpi.raspberrypi.com/books/handbook-2022)
