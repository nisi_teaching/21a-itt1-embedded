---
title: '21A ITT1 EMBEDDED'
subtitle: 'Lecture plan'
filename: '21A_ITT1_EMBEDDED_lecture_plan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
email: 'nisi@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
semester: 21A
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology, ITOiE211/ITOiE212, 21A
* Name of lecturer and date of filling in form: NISI, 2021-06-30
* Title of the course, module or project and ECTS: Embedded systems, 6 ECTS
* Required readings, literature or technical instructions and other background material:  
See "1st semester literature and materials 2021" online at [https://www.ucl.dk/international/full-degree/study-documents#it+technology](https://www.ucl.dk/international/full-degree/study-documents#it+technology)

See the [course website](https://eal-itt.gitlab.io/21a-itt1-embedded) for detailed daily plan, links, references, exercises and so on.


| Lecturer and Content |  Week | Description |
| :--- | :---: | :--- |
|  NISI, Introduction, tools and online resources | 36 | Welcome to the embedded systems course on UCL IT Technology. Today is an introduction to the subject of embedded systems, what is it? what can it be used for and what defiens an embedded system ? We will look at the different ressources used in the course, setup for documentation as well as getting the pieces of knowledge about the first embedded system we will use, the Raspberry Pi Pico. You will be introduced to electronics lab where you have to do a bit of soldering. |
|  NISI, RPi Pico and micropython | 37 | Version control and backups are essential tools in development. This week you need to setup a gitlab project to document and backup all the exercises you do throughout the embedded course. We are going to continue with the Pico and you will write your very first program on the Pico |
|  NISI, RPi Pico Physical computing | 38 | This week you are going to update your knowledge about your RPi Pico's pins and the components that you can connect to them, you are also going to build do your very first embedded program that controls physical components, the "blink" program which is the embedded equivalent to "Hello world" in the programming course. You will also revisit chapter 2 to learn how to use both variables and conditional execution in your embedded programs |
|  NISI, RPi Pico Interfacing to hardware - inputs/outputs | 39 | This week you are goint to expand your RPi Pico with one (or more) external LED's (output device) end also one (or more) buttons (input device)  This week is online.  |
|  NISI, No lectures | 40 | This week has no lectures |
|  NISI, RPi Pico and I2C, BME280 | 41 | This week is your very first glimpse into interfacing a sensor (BME280) using a serial bus and libraries, to your Raspberry Pi Pico. You will learn on a hgh level what a bus is compared to a protocol. OLA15 1st attempt is also this week, this will be an exercise involving what you have learned so far in embedded systems. |
|  NISI, RPi Introduction and setup | 43 |  First week using the Raspberry Pi, make sure you have bought one! The raspberry Pi is a lot more complex compared to the Raspberry Pi Pico, the main difference is that it is using an Operating System based on Debian Linux. You will learn how to setup your RPi headless and also how to secure it with SSH access. You will be presented with different ways of connecting to the RPi. This week is online, if you did not hand in OLA15 this week is 2nd and final attempt |
|  NISI, Catch up  | 44 | This week is without lectures, please use any free time to catch up on exercises from previous weeks. |
|  NISI, RPi physical computing - GPIO and I2C | 45 | The Raspberry Pi has GPIO's just as the Pico has. We will look at interacting with the RPi GPIO using the gpiozero library as well as interfacing the Freenove 1602 LCD display using I2C on the RPi.  |
|  NISI, RPi Interfacing to hardware - UART | 46 | The UART bus is essential when working with embedded systems. We will be using UART to establish 2 way full duplex communication between the RPi and the Pico. The objective is to be able to request bme280 readings on the Pico from the RPi using UART |
|  NISI, RPi Interfacing to hardware - ADC | 47 | We have a probem, the RPi can not read analog signals! It does not have an analog to digital converter (ADC) !! Lucky for us we can communicate with the Pico that, that has an ADC and can read analog signals. We will read a potentiometer and an LDR. If you get far you can also read a thermistor. Of course ADC readings has to be sent to the RPi using UART - This week is online |
|  NISI, Protocols - MQTT and Thingspeak | 48 | Ok, to be fair we do not really have an IoT system yet, we are missing a crusial part which is the I in IoT. In order to send data over the internet we are going to use an IoT protocol called MQ Telemetry Transport (MQTT) to send data to a free cloud solution called Thingspeak, provided by matlab (MathWorks) |
|  NISI, Timers | 49 | Maybe you are not aware about it, but everytime you use the time.sleep() function on either RPi or Pico your device is blocked and cant do anything. Timers and event loops can help us to time task to run periodically. This will free the processors ressources to do other stuff, like reading sensor or doing calculations |
|  NISI, Fundamental IoT system - RPI Pico part | 50 | To be updated. |
|  NISI, Fundamental IoT system - RPI part  | 51 | To be updated. This week is without lecturer. |
|  NISI, Fundamental IoT system - Visualization | 01 | To be updated. |
|  NISI, Exam recap | 02 | To be updated. |

# General info about the course

The subject area is generally concerned with the design, development, testing, documentation and communication of secure, sustainable (embedded) solutions.  

## The student’s learning outcome

### Knowledge  

The student possesses knowledge and understanding of:  

* (LGK1) Communications and interface equipment in general, and how it is applied in selected solutions 
* (LGK2) Electronic modules in overview, and how selected modules are constructed
* (LGK3) Protocols, including communication protocols, their structure and what the differences and application options are 
* (LGK4) Internet of Things technologies, their structure in general and selected solutions in more detail 
* (LGK5) Technical mathematics applied to the subject area as needed to understand electronics and/or communications 
* (LGK6) Operating systems, their characteristics and uses 
* (LGK7) Signal handling in a general sense, and how it is used and incorporated in solutions. 
* (LGK8) Algorithms and design patterns in general and in respect of the selected programming languages
  
### Skills  

The student is able to: 

* (LGS1) Select, adapt and apply embedded systems and components in secure, sustainable solutions 
* (LGS2) Construct and use test systems 
* (LGS3) Document and communicate tasks and solutions using embedded components and systems.  
* (LGS4) Use tools and equipment in the design, development and testing of programs

### Competencies  

The student is able to: 

* (LGC1) Undertake analysis, needs analysis, design, development and testing of secure embedded and sustainable solutions 
* (LGC2) Undertake analysis, diagnosis, testing and maintenance of the technology involved in work with electronic systems, taking account of financial, environmental and quality requirements 
* (LGC3) Acquire new knowledge, skills and competencies in the subject area. 

## Content

The subject component includes: 

* Signal handling
* Component technology
* Communications
* Internet of Things technologies
* Protocols
* Interfacing
* Selection and use of embedded systems and components in integrated solutions.

## Method

### PBL – Problem Based Learning 

A key design factor in the IT Technology programme is the principle that students 
develop better and more relevant competencies by tackling practical problems as 
opposed to working exclusively with theoretical textbooks.  

As a didactic concept, PBL enables the building and maintenance of motivation 
along with the simultaneous development of general and personal competencies.  

PBL is planned on the basis of three key principles: 
* Problem based activity
* Participant engagement in governance
* Situational realism in a learning environment that 
recognises accomplishment and a high level of professional competence (Stegeager, 
Nicolaj, Overgaard Thomassen, Anja, Stentoft, Diana, Egelund Holgaard, Jette et al. 
2020).  

Theory based instruction uses dialogues during presentations and subsequent exercises.   

Various types of instruction and work are alternated, thus connecting the students’ own 
practical experience with theoretical analyses and perspectives.   

New and classic theories and methods are presented for discussion in light of the students’ own practical experience.  

The professional domain is examined in light of studies, development work, 
and new knowledge and research in the area.

### Flipped Learning

Flipped Learning is a pedagogical method that moves instructional learning from a shared environment to an individual one.  

The shared learning environment is transformed into a dynamic and interactive learning environment in which the instructor guides students in the process of learning, understanding, and applying a number of concepts and methods within a given professional subject area. Teaching is one on one, often using online 
resources.  

As a result, class time is freed up so that the instructor may provide, face to 
face with the students, a better and more individual guidance – with the aim of improving the students’ learning experience.  

One of the greatest benefits of flipped learning is the opportunity to customise and individualise the instruction for each student.  

Unfortunately, one of the greatest weaknesses of the method is that success depends on the degree to which students have done the preparatory homework before attending class (Bergmann, 
Jonathan & Sams, Aron 2015: 20-21).  

The schedule in the IT Technology programme includes time slots set aside for the students to prepare for class individually.

### Practical Lab Instruction

Lab work gives the students an opportunity to learn certain skills that are difficult to learn through other means of instruction.  

The IT Technology programme offers 
students an electronics lab and a network lab.  

Lab work supports practical skills like handling equipment and materials, building 
systems, observing and collecting data, analysing and interpreting data as well as skills in collaboration and documentation.  

While such learning opportunities are made available, learning does not automatically occur.  
The instructor must help students learn.  

Lab learning also incorporates the students’ thoughts and observations with respect to the experimental work.  

Troubleshooting by students is required when a practical exercise does not evolve as expected or does not show the anticipated results.  

Lab instruction may, as an example, include:
* A preparatory lecture in which theory is connected to the assignment at hand
* A detailed step-by-step manual for the students to follow
* A final report to be submitted by the students. 

Frequently, the instructor, and perhaps the students as well, are aware of the results of 
an experiment in advance. This type of instruction leaves students with little or no influence on what they are to investigate and how.  

There are benefits to the instructor giving more influence to the students. It increases their learning because more influence requires them to consider purposes, hypotheses, and the design of the experiment and relate these to the scientific concepts.  
In other words, the instructional program may be structured to have the students apply and reflect on professional concepts and knowledge prior to writing a report on the experimental work.  

By giving learning activites a context prior to and during the lab work, instructors give students the opportunity to think about and discuss, in depth, concepts and challenges 
with classmates and the instructor.  

## Equipment

See "1st semester literature and materials 2021" online at [https://www.ucl.dk/international/full-degree/study-documents#it+technology](https://www.ucl.dk/international/full-degree/study-documents#it+technology)

## Projects with external collaborators (proportion of students who participated)
None at this time.

## Test form/assessment
The course includes 1 Obligatory Learning Activity (OLA15).

Assessment of the course is part of the "1st year exam - 1st part" held at the end of 1st semester.

See "1st semester description 2021" for details on obligatory learning activities and examination, online at [https://www.ucl.dk/international/full-degree/study-documents#it+technology](https://www.ucl.dk/international/full-degree/study-documents#it+technology)

## Study activity model

The course activities are divided in 4 categories named K1, K2, K3 and K4.  

The categories are defined as part of the Study Activity Model and is a tool for students to see the workload expected (135 efficient working hours) as well as expectations and responisibility for each category.  

Explanation of each category are in the image below as well as the distribution of activities in both hours and percentage.

In the [weekly plans](https://eal-itt.gitlab.io/21a-itt1-embedded/weekly-plans) students can get an overview of how the categories are distributed in lectures.

![study activity model](Study_Activity_Model.png)

## Other general information
None at this time.
