---
Week: 01
Content: Fundamental IoT system - Firmware and test
Material: See links in weekly plan
Initials: AMNI1
---

# Week 01 - tentative

## Goals of the week(s)

Pratical and learning goals for the period is as follows

### Practical goals

- Use object-oriented programming (OOP) for firmware development
- Test said setup using software testing methods

### Learning goals

- Firmware and OOP
  - Level 1: The student knows what firmware is, and can develop a simple case for the RPi in Python
  - Level 2: The student can use classes and encapsulation to render the firmware invisible to the user.
  - Level 3: The student is able to use software testing to catch errors, and use said info to handle eventual errors.
- Testing
  - Level 1: The student can plan simple test cases and evaluate them using the command line/terminal. 
  - Level 2: The student can use unittest to analyze function outputs.
  - Level 3: The student can use unittest to generate useful test reports to potential users. 

## Schedule

- 8:15 Introduction (K1)
- 9:00 Exercises with lecturer (K1/K4)
- 11:30 Lunch break
- 12:15 Exercises with lecturer (K4)
- 13:00 Exercises without lecturer (K2/K3)
- 15:30 End of day

## Exercises

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww01](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww01)

## Comments

- None
