---
Week: 38
tags:
- RPi Pico
- Physical computing
- GPIO
- Conditional execution
- Variables
---


# Exercises for ww38


## Exercise 1 - Get Started with MicroPython on Raspberry Pi Pico chapter 3

### Information

To learn about the RPi Pico and how to use it we will be using parts of a book from Raspberry Pi foundation called "Get Started with MicroPython on Raspberry Pi Pico".
Each week we will have an exercise to share and consolidate knowledge, it is important that you as a team note this knowledge for each embedded systems lecture. The exercise notes will be invaluable when you at the end of the semester recap knowledge before the exam! 

This is a team exercise.  

*Total: 40 minutes*

### Exercise instructions

1. Individually read chapter 3 "Physical computing" (20 minutes)
2. Download the *Cooperative learning Structures* document from [https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf) (3 minutes)
3. In your team use cooperative learning structure called *Circle of knowledge* to answer the below questions. Note both questions and answers in your teams note document. (15 minutes)

* How do you select the function (input/output/ADC) of a GPIO pin ?
* Can pin 31 - 34 be used as ADC and GPIO simultaneously? 
* What is the purpose of the **RUN** pin?
* How can you damage the GPIO pins on your Pico?
* What is the difference between a momentary switch and a latching switch?
* What is another name for Potentiometer?
* Name the 12 colors a resistor can be coded with, in the correct order, starting from black.

## Exercise 2 - Knowledge sharing (class)

### Information

This exercise shares knowledge between teams using a cooperative learning structure called *Three for tea*.  
Three for tea is a rapid and efficient way to share knowledge, solutions or ideas among teams.  

### Exercise instructions

1. Read the cooperative learning structure called *Three for tea* in the document found at [https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf) (5 minutes)
2. Select a team member to stay at the table (the host), the other teammembers distribute at the 3 other teams tables. (5 minutes)
3. The host presents his/her teams answers to the guests, guests note new knowledge during the presentation. (10 minutes)
4. Guests thanks the host and returns to their team (5 minutes)
5. The team uses *circle of knowledge* to share new knowledge and notes it in the teams note document.(5 minutes)

*Total: 30 minutes*

## Exercise 3 - Your first physical computing program: Hello, LED!

### Information

This exercise is your very first encounter with programming the physical world. It will teach you how to blink the built in LED on the RPi as well as challenging you to explore different ways of blinking the LED.

### Exercise instructions

1. In your team help each other and follow the book chapter 4 instructions in the section named "Your first physical computing program: Hello, LED!" 
2. CHALLENGE: LONGER LIGHT-UP
How would you change your program to make the LED stay on for
longer?  
What about staying off for longer?  
What’s the smallest delay you can use while still seeing the LED switch on and off?
3. Backup your programs to your Gitlab embedded project while you work
4. Note your experiences in your teams note document (you might need that knowledge later)


## Exercise 4 - Conditionals and variables (from chapter 2)

### Information

In the programming lectures this week you are going to learn about conditional execution which fits nicely with an exercise from chapter 2 that we skipped in week 37.  
Don't worry if you did not have a chance to learn this in programming yet, it is also going to make sense to do this exercise before you get the theory behind it.

### Exercise instructions

1. In your team help each other and follow the book chapter 2 instructions in the section named "Conditionals and variables"
2. CHALLENGE 1: ADD MORE QUESTIONS  
Can you change the program to ask more than one question, storing the answers in multiple variables?  
Can you make a program which uses conditionals and comparison operators to print whether a number typed in by the user is higher or lower than 5?  
3. CHALLENGE 2:  
Can you change the program to blink the built in LED 2 times (duration 0.5 seconds for each blink) if the user gives a wrong answer? 
3. Challenge 3: Can you come up with another meaningful way of incorporating the built in LED into your program? 
3. Backup your programs to your Gitlab embedded project while you work
4. Note your experiences in your teams note document (you might need that knowledge later)
