---
Week: 51
Content: IoT basic system build
Material: See links in weekly plan
Initials: NISI
---

# Week 51 - tentative

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Same as week 50

### Learning goals
* same as week 50

## Schedule

* 8:15 Exercises without lecturer (K2/K3)
* 11:30 Lunch break
* 12:15 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Exercises

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww51](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww51)

## Comments

* If you ***did*** complete the IoT basic system build, please use this week to cathch up on missed activities from embedded systems.  
Exercises week 02 can help you in getting an overview of what you need to recap. [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww51](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww51)
* If you ***did not*** complete the IoT basic system build in week 50, then please complete it this week.