---
Week: 49
Content: Timers
Material: See links in weekly plan
Initials: NISI
---

# Week 49 - Timers

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Timer implemented in embedded system
* Exercises completed and documented on gitlab

### Learning goals
* Timers
    * The student knows what a timer is and it's functionality on a conceptual level
    * The student can implement timer examples on the RPi and the RPi Pico
    * THe student can evaluate an existing embedded system and implement timers where appropiate 

## Schedule

* 8:15 Introduction (K1)
* 9:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises with lecturer (K4)
* 13:00 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Exercises

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww49](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww49)

## Comments
* None
