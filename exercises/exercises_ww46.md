---
Week: 46
tags:
- RPi
- Pico
- UART
- Serial communication
---


# Exercises for ww46

## Exercise 1 - UART knowledge

### Information

UART stands for Universal asynchronous receiver-transmitter and is a serial protocol to communicate between two devices.  

Both the RPi Pico and the RPi has a UART interface that you can use from micropython on the Pico and regular Python on the RPi.

UART is built in to micropython and is easy to implement, the RPi needs some additional setup as well as a third party library to enable UART.

This exercise introduces theory about UART as well as pointing to ressources on how to use UART from Python on the RPi and micropython on the Pico.

### Exercise instructions

1. Read this article about UART [https://www.analog.com/en/analog-dialogue/articles/uart-a-hardware-communication-protocol.html](https://www.analog.com/en/analog-dialogue/articles/uart-a-hardware-communication-protocol.html)
2. Read the RPi serial ports documentation [https://docs.bitscope.com/pi-serial/](https://docs.bitscope.com/pi-serial/)
3. Read the documentation for the RPi pyserial library short introduction [https://pyserial.readthedocs.io/en/latest/shortintro.html#](https://pyserial.readthedocs.io/en/latest/shortintro.html#)
4. Read about UART on the RPi Pico [https://docs.micropython.org/en/latest/rp2/quickref.html#uart-serial-bus](https://docs.micropython.org/en/latest/rp2/quickref.html#uart-serial-bus)


## Exercise 2 - UART knowledge sharing

### Information

In your team agree on answers to the below questions and note them in your team's shared document.
This exercise is concluded with a conversation on class about your answers.

### Exercise instructions

1. What is UART an abbreviation of ?
2. Which physical connections does UART require
4. what does uart.readline() do ?
5. What character should all UART writes end with ? (hint! it is an escaped character)
5. What is BAUD rate ?
6. What is stopbits ?
7. What is parity ?
8. Write the names of the 2 UART interfaces on the RPi
9. Write the RPi UART GPIO numbers

### Links

If needed put links here

## Exercise 3 - Setup RPi serial interface 

### Information

The RPi requires a few things to be set up in order to use UART.  
This exercise instructs you in setting it up. 

### Exercise instructions

1. log in to your RPi using SSH (remember to forward the ssh agent with -A)
2. Open raspi-config with `sudo raspi-config`
3. Go to `3. Interface options`
4. Choose `P6 Serial Port`
5. Choose `No` to `login shell over serial`
6. Choose `Yes` to `would you like the serial port hardware to be enabled`
7. Exit raspi-config
7. Disable bluetoth and enable uart1 in `/boot/config.txt` with `sudo nano /boot/config.txt`  
    * Uncomment or add `dtoverlay=disable-bt` in the `[All]` section  
    * Uncomment or add `enable_uart=1` in the `[All]` section

    example:

    ```
    [all]
    #dtoverlay=vc4-fkms-v3d
    dtoverlay=disable-bt
    enable_uart=1
    ```

Read more about the above at 
* [https://docs.bitscope.com/pi-serial/#](https://docs.bitscope.com/pi-serial/#)   
and 
* [https://www.raspberrypi.com/documentation/computers/configuration.html#cm-cm-3-cm-3-and-cm-4](https://www.raspberrypi.com/documentation/computers/configuration.html#cm-cm-3-cm-3-and-cm-4)

## Exercise 4 - Pico to RPi one way communication 

### Information

This exercise is an example on setting up a simple one way UART communication where the Pico periodically sends data collected from the BME280 over UART to the RPi

### Exercise instructions

1. Attach the BME280 to your RPi Pico as described at [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww41](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww41)
2. Attach the Pico UART pins to the RPi UART pins according to the below image  

    ![pico_bme280_RPi_UART.png](pico_bme280_RPi_UART.png)

3. On the RPi, in your gitlab embedded exercises project folder, create a new folder named `rpi_uart` and navigate to it
4. Activate your virtual environment
5. Install pyserial in the virtual environment `python3 -m pip install pyserial` or `pip3 install pyserial`
6. Analyze the below code and then run it on the RPi (create a new python file called `pico_rpi_serialtest.py`)  

    ```py
    import serial
    from time import sleep

    try:
        with serial.Serial('/dev/serial0', 9600) as ser:
            while(True):
                x = ser.readline()
                x = x.decode('utf-8')
                x = x[1:-2]
                print(f'type: {type(x)}, message: {x}')
                sleep(0.5)
    except KeyboardInterrupt:
        print('program closed.....')
    ```

7. Analyze, then run this program on the RPi Pico in a file named `bme280_UART_oneway.py`

    ```py
    # BME280 library https://github.com/SebastianRoll/mpy_bme280_esp8266

    from machine import SoftI2C, Pin, UART
    import bme280 as bme280 # the bme280 library 
    from utime import sleep

    i2c = SoftI2C(scl=Pin(<your_gpio>), sda=Pin(<your_gpio>), freq=10000)

    #Print out any i2c addresses found
    # devices = i2c.scan()
    # if devices:
    #     for d in devices:
    #         print(hex(d)

    uart0 = UART(0, baudrate=9600, tx=Pin(16), rx=Pin(17))     
    bme = bme280.BME280(i2c=i2c, address=0x77)
    led_onboard = machine.Pin(25, machine.Pin.OUT)


    def led_blink(led, times, freq):
        for i in range(times):
            led.value(1)
            sleep(freq)
            led.value(0)
            sleep(freq)
            
    led_blink(led=led_onboard, times=3, freq=0.1)

    while(True):
        
        uart0.write(str(bme.values) + '\n')
        sleep(2)
        led_blink(led=led_onboard, times=1, freq=0.2)
    ```

* If everything is working, you should see values from the bme280 in the RPi CLI.  
* If not, read the instructions again and troubleshoot

### Links

* The above code examples can be found at [https://gitlab.com/ucl-itt-embedded-systems](https://gitlab.com/ucl-itt-embedded-systems)

## Exercise 5 - Pico to RPi one way communication + 1602 LCD display

### Information

This exercise builds on exercise 4, expanding it to show the bme280 values on the 1602 LCD display from the freenove kit

### Exercise instructions

1. On the Rpi copy `pico_rpi_serialtest.py` to a new file using `cp pico_rpi_serialtest.py pico_rpi_serialtest_1602.py`
More info on the `cp` command here: [https://linuxize.com/post/cp-command-in-linux/](https://linuxize.com/post/cp-command-in-linux/) 
2. Open `pico_rpi_serialtest_1602.py` in nano and add the necessary functionality to display the BME valueson the 1602 LCD display (use your 1602 display programs from week 45)

### Links

1602 LCD display example [https://gitlab.com/npes-py-experiments/rpi4_lcd1602_freenove](https://gitlab.com/npes-py-experiments/rpi4_lcd1602_freenove)

## Exercise 6 - Pico to RPi two way communication with command

### Information

In this exercise you will update your Pico code to be able to send BME280 reading when requested via a command sent from the RPi.  
This is using both UART read and write meaning communication two ways.
The exercise builds on the previous exercise in terms of installed libraries as well as the virtual environment.

### Exercise instructions

1. On the RPi create a new file called `pico_rpi_serialtest_cmd.py`
2. In the file add the below code:  

    ```py
    import serial
    from time import sleep

    def read_uart(connection):
        msg = connection.readline() or bytes('error')
        msg = msg.decode('utf-8').rstrip()
        return msg

    def write_uart(connection, message):
        message = message + '\n'
        connection.write(bytes(message, 'utf-8'))

    try:
        with serial.Serial(port='/dev/serial0', baudrate=19200, bytesize=8, parity='N', stopbits=1) as ser:
            while True:
                write_uart(ser, 'temperature')
                sleep(1)
                print(read_uart(ser))
                write_uart(ser, 'humidity')
                sleep(1)
                print(read_uart(ser))
                write_uart(ser, 'pressure')
                sleep(1)
                print(read_uart(ser))

    except KeyboardInterrupt:
        print('program closed.....')
    ```

3. Analyze the code to get familiar with it
4. On the Pico add a file called `bme280_UART_twoway_cmd.py` 
5. In the file add the below code:  

    ```py
    # BME280 library https://github.com/SebastianRoll/mpy_bme280_esp8266
    # The library needs to be put in the lib folder of the Pico
    # To run this file on Pico boot save it on the Pico as main.py (outside of the lib folder)

    from machine import SoftI2C, Pin, UART
    import bme280 as bme280 # the bme280 library 
    from utime import sleep

    i2c = SoftI2C(scl=Pin(<your_gpio>), sda=Pin(<your_gpio>), freq=10000)

    #Print out any addresses found
    # devices = i2c.scan()
    # if devices:
    #     for d in devices:
    #         print(hex(d)

    uart0 = UART(0, 19200, bits=8, parity=None, stop=1, tx=Pin(16), rx=Pin(17))     
    bme = bme280.BME280(i2c=i2c, address=0x77)
    led_onboard = machine.Pin(25, machine.Pin.OUT)


    def led_blink(led, times, freq):
        for i in range(times):
            led.value(1)
            sleep(freq)
            led.value(0)
            sleep(freq)
            
    led_blink(led=led_onboard, times=3, freq=0.1)

    while(True):
        (temperature, pressure, humidity) = bme.values # unpack tuple to variables
        # print(str(bme.values) + '\n') # development debug message

        if uart0.any() > 0: # check if anything has been received on UART, Returns the number of bytes waiting (may be 0)
            msg = uart0.readline() or b'' # read UART, if nothing received set msg to empty byte object
            msg = msg.decode('utf-8').rstrip() #decode byte object and remove newline character
            if msg == 'temperature':
                uart0.write((temperature) + '\n')
                print(temperature)
                led_blink(led=led_onboard, times=1, freq=0.2)
            elif msg == 'pressure':
                uart0.write((pressure) + '\n')
                print(pressure)
                led_blink(led=led_onboard, times=2, freq=0.2)
            elif msg == 'humidity':
                uart0.write((humidity) + '\n')
                print(humidity)
                led_blink(led=led_onboard, times=3, freq=0.2)
            else:
                uart0.write(('error') + '\n')
                led_blink(led=led_onboard, times=5, freq=0.05)
    ```

6. Analyze the code to get familiar with it
7. On the Pico run `bme280_UART_twoway_cmd.py`
8. On the RPi run `pico_rpi_serialtest_cmd.py`

    * If everything is working, you should see `temperature`, `humidity` and `pressure` values from the bme280 in the RPi CLI, on seperate lines.  
    * If not, read the instructions again and troubleshoot

9. Modify `pico_rpi_serialtest_cmd.py` to display the values on the 1602 LCD display

## Exercise 7 - Pico to RPi two way communication with buttons

### Information

Eercise 5 is using commands to get bme280 readings via UART from the Pico.  
It makes more sense if the reading is triggered by a button press on the RPi. 

### Exercise instructions

1. Attach 3 buttons between ground and 3 different GPIO pins on the RPi
2. On the RPi copy `pico_rpi_serialtest_cmd.py` to a new file called `pico_rpi_serialtest_buttons.py` using the `cp` command
3. Modify `pico_rpi_serialtest_buttons.py` to trigger each of the commands from seperate buttons.

    Use the below flowchart to modify: 

    ![ww46_pico_rpi_serialtest_buttons.py.png](ww46_pico_rpi_serialtest_buttons.py.png)

* Test and tweak the code to be as responsive as possible
* Troubleshoot if necessary