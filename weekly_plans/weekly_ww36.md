---
Week: 36
Content: Introduction, tools and online resources
Material: See links in weekly plan
Initials: NISI
---

# Week 36

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Solder pin headers to RPi Pico
* Install microPython on RPi Pico

### Learning goals

* Get Started with MicroPython on Raspberry Pi Pico chapter 1 (LGK2, LGK6, LGS1, LGS2)
    * Level 1: The student has basic knowledge about an embedded system (RPi Pico)
    * Level 2: The student knows the basic operations of an embedded system
    * Level 3: The student knows how to load firmware onto an embedded system

* Prepare RPi Pico Hardware (LGK2, LGS1, LGS2)
    * Level 1: The student knows the layout of an embedded PCB
    * Level 2: The student can identify correct components for an embedded system (pin headers)
    * Level 3: The student can prepare an embedded system for use on a breadboard (solder pin headers)

* Intall microPython on RPi Pico (LGK6, LGS2, LGS3, LGC3)
    * Level 1: The student can install firmware on an embedded system
    * Level 2: The student can guide others in installing firmware on an embedded system  
    * Level 3: The student can, in a team setting, document own work



## Schedule

* 8:15 Introduction (K1)
* 9:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises with lecturer (K4)
* 13:45 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Exercises

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww36](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww36)

## Comments
* none
