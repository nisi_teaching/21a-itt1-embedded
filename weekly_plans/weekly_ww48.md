---
Week: 48
Content: MQTT and Thingspeak
Material: See links in weekly plan
Initials: NISI
---

# Week 48 - MQTT and Thingspeak

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Sensor data displayed on Thingspeak
* Exercises completed and documented on gitlab

### Learning goals
* MQTT
    * Level 1: The student knows what MQTT is and what programming design pattern it follows
    * Level 2: The student can setup a boilerplate example of using MQTT in conjunction with the Thingspeak MQTT broker and dashboard
    * Level 3: The student can implement MQTT in an embedded solution, to visualize sensor data, using the Thingspeak MQTT broker and dashboard

## Schedule

* 8:15 Introduction (K1)
* 9:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises with lecturer (K4)
* 13:00 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Exercises

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww48](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww48)

## Comments
* None
